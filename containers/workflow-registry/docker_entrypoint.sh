port=8002
exec_path=/workflow-registry

# Registry db initializations

until python $exec_path/manage.py makemigrations > /dev/null 2>&1 ;do
      echo "Waiting mysql docker to setup........"
      sleep 1
done

echo "Initializing....."
python $exec_path/manage.py makemigrations
python $exec_path/manage.py migrate
python $exec_path/manage.py migrate --run-syncdb
python $exec_path/manage.py collectstatic

username="root"
password="root"

email=$username"@example.com"

echo "Creating super user....."

echo "from django.contrib.auth.models import User; User.objects.create_superuser('$username', '$email', '$password')" | python $exec_path/manage.py shell

echo "Starting web server...."

# python $exec_path/manage.py runserver 0.0.0.0:$port
cd $exec_path
exec gunicorn  -w 9 -b 0.0.0.0:$port workflow_registry.wsgi \
		--log-level debug \
                --backlog 0 \
                --timeout 120
