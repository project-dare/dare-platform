#!/usr/bin/env bash

echo "ENSAMBLE_INDEX" $ENSAMBLE_INDEX

SPROV_IMPORT_ENDPOINT="http://sprov:8082/workflowexecutions/import"
PROVENANCE_DIR="PROVENANCE"

cwl-runner --provenance ${PROVENANCE_DIR} --enable-user-provenance  --enable-host-provenance --full-name ${USER_ID}@${ISSUER} run_test.cwl run_test.yml
# cwltool --debug --tmp-outdir-prefix=./tmp/ --tmpdir-prefix=./tmp/ run_test.cwl run_test.yml

#RESULT_LOCATION=`pwd`       # For now the result location is the current directory. This should be changed to an URL.
PROVENANCE_ZIP=${PROVENANCE_DIR}.zip
zip -r output/${PROVENANCE_ZIP} PROVENANCE/metadata/provenance

# curl -X POST ${SPROV_IMPORT_ENDPOINT} -H "accept: application/json" -H "Content-Type: multipart/form-data" \
#      -F "archive=@output/${PROVENANCE_ZIP};type=application/zip" -F "format=CWLProv" -F "resultLocation=$RESULT_LOCATION" \
#      -F "runId=${RUN_ID}" -H "Authorization: OAuth ${ACCESS_TOKEN}"
curl -X POST ${SPROV_IMPORT_ENDPOINT} -H "accept: application/json" -H "Content-Type: multipart/form-data" \
     -F "archive=@output/${PROVENANCE_ZIP};type=application/zip" -F "format=CWLProv" -F "resultLocation=$RESULT_LOCATION" \
     -F "runId=${RUN_ID}-${ENSAMBLE_INDEX}" -H "Authorization: OAuth ${ACCESS_TOKEN}"
#rm -f ${PROVENANCE_ZIP}
# cp ${PROVENANCE_ZIP} ${EXEC_PATH}/output/.
# ls -alF ${EXEC_PATH}/output/.
