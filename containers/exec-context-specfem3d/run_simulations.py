#!/usr/bin/env python
import glob
import json
import os
import shutil
import subprocess
from os import environ
from os import mkdir, getcwd
from os.path import join, exists

folder_name = json.loads(environ["INPUT_DATA"])["folder_name"]
filename = json.loads(environ["INPUT_DATA"])["filename"]
username = environ["USERNAME"]
rundir = environ["RUN_DIR"]
full_path = "/home/mpiuser/sfs/{}/uploads/{}/{}".format(username, folder_name, filename)
print("full_path ",full_path)
json_content = json.load(open(full_path))
UseCase = json_content['execution label']
print('UseCase ',UseCase)

#input dir should be /WP6_EPOS/specfem3d/specfem3d_input/OUTPUT_FILES/specfem/fmagnoni/specfem3d_test_input/DATA
input_data_folder = join(getcwd(), "specfem3d_test_input", "DATA") 
tmp_out_dir = join(getcwd(), "results", "OUTPUT_FILES") 
tmp_der_dir = join(tmp_out_dir,'deriv')
synth_dir_name = 'synth'

uploads_folder = "/home/mpiuser/sfs/{}/uploads".format(username)

try:
    os.mkdir(join(uploads_folder, "wf_data"))
except:
    pass

##RAPID ASSESSMENT test case
if UseCase == 'RA':
    in_simul = json_content[UseCase]['cmt_simulation_par']['run_simulation']
    if in_simul:
        if in_simul[0] == "initial":
            # check input files are named as needed by specfem
            mesh = json_content[UseCase]['SPECFEM3D']['mesh']
            vel = json_content[UseCase]['SPECFEM3D']['vel']
            parfile = json_content[UseCase]['SPECFEM3D']['parameter file']
            stations = json_content[UseCase]['SPECFEM3D']['station file']
            # can be a list of events to be simulated
            cmts = json_content[UseCase]['SPECFEM3D']['events']['cmtsolution']
            print("initial cmts are: ",len(cmts))
            # check input files are named as needed by specfem
            if mesh != "mesh":
                os.rename(join(input_data_folder,mesh), join(input_data_folder,"mesh"))
            if vel != "velocity":
                os.rename(join(input_data_folder,vel), join(input_data_folder,"velocity"))
            if parfile != "Par_file":
                os.rename(join(input_data_folder,parfile), join(input_data_folder,"Par_file"))
            if stations != "STATIONS":
                os.rename(join(input_data_folder,stations), join(input_data_folder,"STATIONS"))
            #
            counter = 0
            for cmt in cmts:
                # create output dirs
                # synth_dir='./synth_'+cmt+'/'
                # synth_dir = './synth/'
                if len(cmts) == 1:
                    # synth_dir='./synth/'
                    # synth_dir='synth'
                    synth_dir=synth_dir_name
                else:
                    #synth_dir='./synth_'+cmt+'/'
                    # synth_dir='synth_'+cmt
                    synth_dir=synth_dir_name+'_'+cmt
                # check input files are named as needed by specfem
                if cmt != "CMTSOLUTION":
                    os.rename(join(input_data_folder,cmt), join(input_data_folder,"CMTSOLUTION"))
                #execute specfem
                os.environ["ENSAMBLE_INDEX"] = str(counter)
                print("LAUNCH SPECFEM simul for RA with input: " + cmt + ", Par_file, STATIONS, mesh_vel.zip")
                command = "bash exec_specfem.sh"
                process = subprocess.Popen(command, env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) #FM: check the env var
                # process.wait()
                print('wait returned')             
                # print('OUT & ERROR', process.stdout.read())
                for line in iter(process.stdout.readline, b''):
                    print(line)
                # output, error = process.stdout, process.stderr
                # print('output',output)
                # print('error',error)
                ## AFTER SIMULATION
                # mv *.sem? in a temporary synth dir
                # base_path = "/home/mpiuser/sfs/{}/runs/{}".format(username, rundir) #21/10/20
                # os.chdir(base_path) #21/10/20
                #tmp_out_dir = join(getcwd(), "results", "OUTPUT_FILES")
                tmp_synth_dir = join(tmp_out_dir,synth_dir)
                if not os.path.exists(tmp_synth_dir):
                    os.mkdir(tmp_synth_dir)
                #
                for seismos in glob.glob(join(tmp_out_dir,"*.sem?")):
                    seismo = seismos.split("/")[-1]
                    shutil.move(seismos,join(tmp_synth_dir,seismo))
                # backup input files
                print('backup input files')
                os.rename(join(input_data_folder,"CMTSOLUTION"), join(input_data_folder,cmt))
                shutil.copyfile(join(input_data_folder,cmt), join(tmp_synth_dir, cmt))
                shutil.copyfile(join(input_data_folder,"STATIONS"), join(tmp_synth_dir, stations))
                shutil.copyfile(join(input_data_folder,"Par_file"), join(tmp_synth_dir, parfile))
                # move *.sem? in the final synth dir required by following d4p wfs
                # uploads_folder = "/home/mpiuser/sfs/{}/uploads".format(username)
                # if not exists(join(uploads_folder, "wf_data")):
                #     mkdir(join(uploads_folder, "wf_data"))
                #########
                print('organise output')
                if exists(join(uploads_folder, "wf_data/synth")):
                    for seismos in glob.glob(join(tmp_synth_dir,"*.sem?")):
                        seismo = seismos.split("/")[-1]
                        shutil.move(seismos,join(uploads_folder,"wf_data/synth",seismo))                        
                else:
                    shutil.move(src=tmp_synth_dir, dst=join(uploads_folder, "wf_data"))
                #
                counter=counter+1    
            os.rename(join(input_data_folder,"mesh"), join(input_data_folder,mesh))
            os.rename(join(input_data_folder,"velocity"), join(input_data_folder,vel))
            os.rename(join(input_data_folder,"Par_file"), join(input_data_folder,parfile))
            os.rename(join(input_data_folder,"STATIONS"), join(input_data_folder,stations))
        else:
            print("Bad parameter setting for RA!")
    elif in_simul is None:
        print("Simulation not needed (synthetics for the chosen event are already available)")

##MOMENT TENSOR IN 3D test case
elif UseCase == 'MT3D':

    MTe_notation = ['initial', 'Mrr', 'Mtt', 'Mpp', 'Mrt', 'Mrp', 'Mtp', 'depth', 'latorUTM', 'longorUTM']
    simuls = json_content[UseCase]['cmt_simulation_par']['run_simulation']
    print(simuls)
    #
    if simuls:
        print("simulate only the specified synt or deriv synt: " + str(simuls))
        # check input files are named as needed by specfem
        mesh = json_content[UseCase]['SPECFEM3D']['mesh']
        vel = json_content[UseCase]['SPECFEM3D']['vel']
        parfile = json_content[UseCase]['SPECFEM3D']['parameter file']
        stations = json_content[UseCase]['SPECFEM3D']['station file']
        if mesh != "mesh":
            os.rename(join(input_data_folder,mesh), join(input_data_folder,"mesh"))
        if vel != "velocity":
            os.rename(join(input_data_folder,vel), join(input_data_folder,"velocity"))
        if parfile != "Par_file":
            os.rename(join(input_data_folder,parfile), join(input_data_folder,"Par_file"))
        if stations != "STATIONS":
            os.rename(join(input_data_folder,stations), join(input_data_folder,"STATIONS"))

        if simuls[0] == 'all':
            simuls = MTe_notation
        if simuls[0] == 'MTs':
            simuls = MTe_notation[1:7]
        if simuls[0] == 'locs':
            simuls = MTe_notation[8:10]
        if simuls[0] == 'MTs+dep':
            simuls = MTe_notation[1:8]
        if simuls[0] == 'MTs+locs':
            simuls = []
            simuls = [None] * 6
            simuls[0:5] = MTe_notation[1:7]
            simuls[6:7] = MTe_notation[8:10]
        if simuls[0] == 'dep+locs':
            simuls = MTe_notation[7:10]
        print(simuls)
        ##choose the specific cmt
        counter = 0
        # print('counter initial ', counter)
        for MTi in simuls:
            if isinstance(MTi, int):
                MTi -= 1  # json setting 1: initial, python setting 1: Mrr
                print(MTi)
                MTi = MTe_notation[MTi]
                print(MTi)
            else:
                print(MTi)
            if MTi == 'initial':
                # can be a list of events to be simulated but less used for MT3D
                cmts = json_content[UseCase]['SPECFEM3D']['events']['cmtsolution']
                print("initial cmts are: ",len(cmts))
                for cmt in cmts:
                    if len(cmts) == 1:
                        synth_dir=synth_dir_name
                    else:
                        synth_dir=synth_dir_name+'_'+cmt
                    ##check input files are named as needed by specfem
                    if cmt != "CMTSOLUTION":
                        os.rename(join(input_data_folder,cmt), join(input_data_folder,"CMTSOLUTION"))
                    # execute specfem
                    # print('counter for synth ', counter)
                    os.environ["ENSAMBLE_INDEX"] = str(counter)
                    print("LAUNCH SPECFEM simul for MT3D with input: Par_file, STATIONS, mesh_vel.zip and starting " + cmt)
                    command = "bash exec_specfem.sh"
                    process = subprocess.Popen(command, env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) 
                    # process.wait()
                    # output, error = process.stdout, process.stderr
                    print('wait returned')
                    # print('OUT & ERROR', process.stdout.read())
                    for line in iter(process.stdout.readline, b''):
                        print(line)                    
                    ## AFTER SIMULATION
                    # mv *.sem? in a temporary synth dir
                    # base_path = "/home/mpiuser/sfs/{}/runs/{}".format(username, rundir) #21/10/20
                    # os.chdir(base_path) #21/10/20
                    #tmp_out_dir = join(getcwd(), "results", "OUTPUT_FILES")
                    tmp_synth_dir = join(tmp_out_dir,synth_dir)
                    print('tmp_synth_dir', tmp_synth_dir)
                    if not os.path.exists(tmp_synth_dir):
                        os.mkdir(tmp_synth_dir)
                    for seismos in glob.glob(join(tmp_out_dir,"*.sem?")):
                        seismo = seismos.split("/")[-1]
                        shutil.move(seismos,join(tmp_synth_dir,seismo))                    
                    # backup input files (for API set dest dir as the specific  run_* output dir)
                    print('backup input files')
                    os.rename(join(input_data_folder,"CMTSOLUTION"), join(input_data_folder,cmt))
                    shutil.copyfile(join(input_data_folder,cmt), join(tmp_synth_dir, cmt))
                    shutil.copyfile(join(input_data_folder,"STATIONS"), join(tmp_synth_dir, stations))
                    shutil.copyfile(join(input_data_folder,"Par_file"), join(tmp_synth_dir, parfile))
                    # move *.sem? in the final synth dir required by following d4p wfs
                    # if not exists(join(uploads_folder, "wf_data")):
                    #     mkdir(join(uploads_folder, "wf_data"))
                    print('organise synth output')
                    if exists(join(uploads_folder, "wf_data/synth")):
                        for seismos in glob.glob(join(tmp_synth_dir,"*.sem?")):
                            seismo = seismos.split("/")[-1]
                            shutil.move(seismos,join(uploads_folder,"wf_data/synth",seismo))                        
                    else:
                        shutil.move(src=tmp_synth_dir, dst=join(uploads_folder, "wf_data"))
                    #
                    counter=counter+1 
                    # print('counter for synth increm ', counter)
            if MTi != 'initial':
                if MTi == 'depth':
                    MTi = 'dep'
                elif MTi == 'latorUTM':
                    MTi = 'lat'
                elif MTi == 'longorUTM':
                    MTi = 'lon'
                cmt = "CMTSOLUTION_" + MTi
                ##remove suffix from CMTSOLUTION file
                os.rename(join(input_data_folder,cmt), join(input_data_folder,"CMTSOLUTION"))
                # execute specfem
                # print('counter for deriv ', counter)
                os.environ["ENSAMBLE_INDEX"] = str(counter)
                print("LAUNCH SPECFEM simul for MT3D with input: Par_file, STATIONS, mesh_vel.zip and cmt " + cmt)
                command = "bash exec_specfem.sh"
                process = subprocess.Popen(command, env=os.environ, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) #FM: check the env var
                # process.wait()
                print('wait returned')            
                # print('OUT & ERROR', process.stdout.read())
                for line in iter(process.stdout.readline, b''):
                    print(line)
                # output, error = process.stdout, process.stderr
                ## AFTER SIMULATION
                ## mv *.sem? in a temporary deriv synth dir assigning suffix MTi
                # base_path = "/home/mpiuser/sfs/{}/runs/{}".format(username, rundir) #21/10/20
                # os.chdir(base_path) #21/10/20
                # tmp_der_dir = join(tmp_out_dir,'deriv')
                if not os.path.exists(tmp_der_dir):
                    os.mkdir(tmp_der_dir)   
                for seismos in glob.glob(join(tmp_out_dir,"*.sem?")):
                    seismo = seismos.split("/")[-1]
                    seismo_der = seismo + '.' + MTi
                    shutil.move(seismos,join(tmp_der_dir,seismo_der))                             
                # backup input files (for API set dest dir as the specific  run_* output dir)
                print('backup input files')
                os.rename(join(input_data_folder,"CMTSOLUTION"), join(input_data_folder,cmt))
                shutil.copyfile(join(input_data_folder,cmt), join(tmp_der_dir,cmt))
                shutil.copyfile(join(input_data_folder,"STATIONS"), join(tmp_der_dir,stations))
                shutil.copyfile(join(input_data_folder,"Par_file"), join(tmp_der_dir,parfile))
                #
                counter=counter+1
                # print('counter for deriv increm ', counter)
        # move deriv/*.sem?.MTi in the final der synth dir required by following d4p wfs
        # if not exists(join(uploads_folder, "wf_data")):
        #     mkdir(join(uploads_folder, "wf_data"))
        if exists(tmp_der_dir):
            print('organise deriv output') 
            if exists(join(uploads_folder, "wf_data/deriv")):
                for seismos in glob.glob(join(tmp_der_dir,"*.sem*")):
                    seismo = seismos.split("/")[-1]
                    shutil.move(seismos,join(uploads_folder,"wf_data/deriv",seismo))                        
            else:
                shutil.move(src=tmp_der_dir, dst=join(uploads_folder, "wf_data"))                         
        # backup input files (for API set dest dir as the specific  run_* output dir)
        os.rename(join(input_data_folder,"mesh"), join(input_data_folder,mesh))
        os.rename(join(input_data_folder,"velocity"), join(input_data_folder,vel))
        os.rename(join(input_data_folder,"Par_file"), join(input_data_folder,parfile))
        os.rename(join(input_data_folder,"STATIONS"), join(input_data_folder,stations))        
    else:
        print("all synthetics and derivative synthetics are already available")
        
else:
    print("The test case should be specified!")
