#!/usr/bin/env bash
set -x 

# prepare directories for logs used by EXEC-API
EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
cd "${EXEC_PATH}"

# copy files from uploads
mkdir -p specfem3d_test_input
#cp -r "/home/mpiuser/sfs/${USERNAME}/uploads/DATA" ./specfem3d_test_input/
rsync -r "/home/mpiuser/sfs/${USERNAME}/uploads/DATA" ./specfem3d_test_input/
rsync -c -r "/home/mpiuser/sfs/${USERNAME}/uploads/DATA" ./specfem3d_test_input/
mv ./specfem3d_test_input/DATA/mesh_vel/mesh ./specfem3d_test_input/DATA/mesh_vel/velocity ./specfem3d_test_input/DATA/mesh_vel/mesh_vel_model.rtf ./specfem3d_test_input/DATA/
rm -rf ./specfem3d_test_input/DATA/mesh_vel

# execution
# find /home/mpiuser/docker -maxdepth 1 -type f -print0 -exec /bin/cp -f -- "{}" . \;
# cp /home/mpiuser/docker/* .
rsync /home/mpiuser/docker/* .
rsync -c /home/mpiuser/docker/* .
python3 -u run_simulations.py 2>&1

find "/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}" -maxdepth 1 -user mpiuser -exec chmod 777 -R {} \;
# chmod -R 777 "/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
# rm -rf tmp/
#cp "${EXEC_PATH}/output/PROVENANCE.zip" "/home/mpiuser/sfs/${USERNAME}/uploads/wf_data/${RUN_DIR}_PROVENANCE.zip"

