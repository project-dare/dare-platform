#! /bin/sh

# Locale has to be set, otherwise Click assumes ASCII and Flask will not start:
export LC_ALL=C.UTF-8
export LANG=C.UTF-8

python ./queue_reader.py &
gunicorn --reload -w 9 -b 0.0.0.0:5000 "prov_queue:main()" --chdir /s-ProvFlow-1.2.7/prov-queue/ --log-level debug --backlog 0 --timeout 120 --error-logfile - --log-file -
