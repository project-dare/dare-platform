#! /bin/bash

set -e
OUTDIR="output"
ANALYTICS="analytics"
EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
mkdir -p "${EXEC_PATH}/${ANALYTICS}"
cd "${EXEC_PATH}"

# execute the cwl workflow
cwl-runner --tmp-outdir-prefix="${EXEC_PATH}/${OUTDIR}" --tmpdir-prefix="${EXEC_PATH}/${OUTDIR}" "${WORKFLOW_NAME}" "${SPEC_NAME}"
