#! /bin/bash
# Create virtual enviroment if specified by user
set -e
EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
cd "${EXEC_PATH}"

# TODO uncomment the following for the MT3D image
# if [ ! -d "pyflex" ] ; then
 #    git clone "https://github.com/fmagnoni/pyflex.git" "pyflex"
   #  pip install -v -e ./pyflex
# fi
# if [ ! -d "spaceweight" ] ; then
#     git clone "https://github.com/wjlei1990/spaceweight.git" "spaceweight"
#     cd spaceweight && python setup.py install && cd ..
# fi
# if [ ! -d "pycmt3d" ] ; then
#     git clone "https://github.com/fmagnoni/pycmt3d.git" "pycmt3d"
#     cd pycmt3d && python setup.py install && cd ..
# fi

if [ ! $USER_REQS == "None" ]
then
    # Creating virtual environment
    python3 -m venv --copies --system-site-packages `echo $USERNAME`_env
    source `echo $USERNAME`_env/bin/activate
    pip install cwl-runner
    pip install cwltool==1.0.20190621234233
    wget "${USER_REQS}"
    pip install --upgrade pip setuptools==45
    # Split string with delimiter "/" and get last split argument
    pip install -I --no-binary basemap -r "${USER_REQS##*/}"
    rm "${USER_REQS##*/}"

    deactivate
    # mpi requirements sharing
    ln -s /home/mpiuser/docker/dispel4py .
    cd dispel4py
    export PATH="${EXEC_PATH}/${USERNAME}_env/bin/"
    python -c "import sys; print(sys.executable)"
    python setup.py install
    source /etc/profile
    cd ..
    unlink dispel4py
fi
