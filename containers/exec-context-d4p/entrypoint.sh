#!/usr/bin/env bash
set -e
EXEC_PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
cd "${EXEC_PATH}"
find /home/mpiuser/docker/* -maxdepth 1 -type f | xargs cp -t .
# Create virtual enviroment if specified by user
OUTDIR="output"

if [ ${USER_REQS} == "None" ]
then
    # Start execution
    while true; do
       if [ ! -z "${D4P_RUN_DATA}" ]; then
          sleep 7 && if [ $(grep 'target:' spec.yaml | cut -c 9-) = "simple" ];then bash start_simple.sh ${OUTDIR} ${PATH}; else bash start_mpi.sh ${OUTDIR} ${PATH};fi && \
          cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
                      cp -p $i/* . && rm -rf $i || rm -rf $i;done
          break
       fi
    done
else
    # Start execution
    while true; do
       if [ ! -z "${D4P_RUN_DATA}" ]; then
              export PATH="/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}/${USERNAME}_env/bin:/usr/local/bin:/bin:/usr/bin:/usr/sbin" && \
              sed -i 's/\(secure_path="\)[^"]*"/\1\/home\/mpiuser\/sfs\/'"${USERNAME}"'\/runs\/'"${RUN_DIR}"'\/'"${USERNAME}"'_env\/bin:\/usr\/local\/bin:\/bin:\/usr\/bin:\/usr\/sbin"/' /etc/sudoers && \
              sleep 7 && if [ $(grep 'target:' spec.yaml | cut -c 9-) = "simple" ];then bash start_simple.sh ${OUTDIR} ${PATH}; else bash start_mpi.sh ${OUTDIR} ${PATH};fi && \
              source /etc/profile && rm -f reqs.txt && \
              rm -rf "${USERNAME}_env" && \
          cd ${OUTDIR} && for i in `ls -d */`;do [ "$(ls -A $i)" ] && \
                      cp -p $i/* . && rm -rf $i || rm -rf $i;done
          break
       fi
    done
fi

chmod -R 777 "/home/mpiuser/sfs/${USERNAME}/runs/${RUN_DIR}"
