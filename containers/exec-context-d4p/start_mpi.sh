#! /bin/bash
chown mpiuser:mpiuser $1
export PATH="$2"
echo "run_path: PATH" >> spec.yaml
sudo -u mpiuser -E cwl-runner --tmp-outdir-prefix=./$1/ --tmpdir-prefix=./$1/ mpiexec-d4py-tool.cwl spec.yaml 
