# Work-in-progress Docker image for the Vulcanology use case using Fall3D.

To test manually, follow these steps:

* build image, e.g. `docker build -t exec-context-fall3dpy .`
* download static input data and unzip to current working directory: 
```
 wget https://owncloud.scai.fraunhofer.de/index.php/s/3RJqXi5k7HEA9TB/download
 unzip WP6-Fall3d.zip
```
* run image interactively using a bind mount for the input data
```
 docker run -it -v `pwd`/WP6-Fall3d:/input:ro exec-context-fall3dpy /bin/bash
```
* the rest of the commands are to be run inside the container.
* link the input folders to the working directory:
```
cd /home/mpiuser/docker/wp6_volcanology
ln -s /input/Cartopy_Data/ .
ln -s /input/TsupyDEM/ .
```
* export PYTHONPATH to fix relative imports 
```
export PYTHONPATH=.:$PYTHONPATH
```
* Start a project, e.g. with Stromboli case:
```
python f3_setup.py Eruption_test_stromboli Stromboli
```
* run download step:
```
dispel4py multi f3_dispel4py_download.py -n 10 -f project_parameters.json
```
* run Simulation step: 
```
python FALL3D_Executor.py
```
* create input_parameters.json with plotting parameters (see notebook in wp6)
```
python set-plotting-parameters.py
```
* run postprocessing step: 
```
dispel4py multi f3_dispel4py_plotting -n 10 -f input_parameters.json
```
* Output will be in the Simulations/.../Figures folder of your project (depending on
the name you chose when calling f3_setup.py). 
