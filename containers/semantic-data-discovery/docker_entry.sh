#! /bin/sh

SOLR_URL="http://${SOLR_PVC_SVC_SERVICE_HOST}:${SOLR_PVC_SVC_SERVICE_PORT}/solr/"
python3 configurate.py solr -u $SOLR_URL -c $SOLR_CORE
python3 configurate.py endpoint -n "data-catalogue" -t "url" $VIRTUOSO_SPARQL

waitress-serve --port 5000 --call app.ui:create_app