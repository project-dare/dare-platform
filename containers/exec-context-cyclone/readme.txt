#
# Bulding the docker
docker build --tag cyclone:1.0 .

# Starting the docker and running the workflow
docker run cyclone:1.0

# To start the docker and enter with bash overriding the entrypoint
docker run -it --entrypoint "/bin/bash" cyclone:1.0

