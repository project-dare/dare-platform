#
# Bulding the docker
docker build --tag cyclone:1.0 .

# Starting the docker
docker run cyclone:1.0

# To start the docker and enter with bash overriding the entrypoint
docker run -it --entrypoint "/bin/bash" cyclone:1.0

# Tagging the image for the DARE platform
docker build --tag registry.gitlab.com/project-dare/dare-platform/cyclone:v1.0 .

# Logging into the gitlab registry for the DARE platform
docker login registry.gitlab.com
Then enter username and app token

# Pushing the docker image to the registry for the DARE platform
docker push registry.gitlab.com/project-dare/dare-platform/cyclone:v1.0

# Jupyter Notebook for the DARE platform
https://gitlab.com/project-dare/exec-api/-/blob/master/examples/wp7/cyclone_tracking/cyclone.ipynb

