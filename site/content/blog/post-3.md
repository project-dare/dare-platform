---
title: DARE platform February 2020 Release
date: 2020-02-07
image: "images/dare_transparent.png"
---

DARE platform February 2020 release is now available at https://testbed.project-dare.eu/

In this version a queue service added to unburden the workflowexecutions/insert sprov-api end point.
Additionally, CWL provenance to s-prov mapping has been improved and sprovflow-viewer search options 
have been expanded. You can now add more search terms in the filter dialog.

To download the v2.2 release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v2.2)
 
