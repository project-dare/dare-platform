---
title: DARE platform v3.4 release
date: 2020-07-27
image: "images/dare_transparent.png"
---

DARE platform v3.4 release is now available at https://platform.dare.scai.fraunhofer.de/

The v3.4 release contains various fixes and updates in workflow execution and monitoring.
The dare-login and exec-api components are improved to handle much more requests at the 
same time during the monitoring of the workflow execution.

To download the v3.4 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.4)
 
