---
title: DARE platform v3.6 release
date: 2020-10-23
image: "images/dare_transparent.png"
---

DARE platform v3.6 release is now available at https://platform.dare.scai.fraunhofer.de/

The v3.6 release contains various fixes and updates in the Execution API component. It's written almost from scratch,
has improved features and easier API. The two Shared File Systems are now integrated into a single one and all the DARE
use cases are updated and follow the same folder structure.

To download the v3.6 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.6)
 
