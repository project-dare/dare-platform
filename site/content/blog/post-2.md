---
title: DARE platform January 2020 Release
date: 2020-01-14
image: "images/dare_transparent.png"
---

DARE platform January 2020 release is now available at https://testbed.project-dare.eu/

A new testing environment is now included in the platform. Use our toy example to get familiar with the new functionalities.

Additionally, the DARE platform's shared file system is re-organized and more user-oriented. 
Users can view/download their generated files by interacting with the DARE's execution API. 
Read more information check the Features section. To download the v2.1 release of the DARE platform
visit our [GitLab Repository](https://gitlab.com/project-dare/dare-platform/tree/v2.1)
 
