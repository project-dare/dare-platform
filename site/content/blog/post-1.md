---
title: DARE platform October 2019 Release
date: 2019-10-21
image: "images/dare_transparent.png"
---

DARE platform October 2019 release is now available at https://testbed.project-dare.eu/. 

The DARE Execution API is now integrated in the DARE platform. All the necessary files for
Docker and Kubernetes are added in the new DARE platform 
release in our [GitLab Repository](https://gitlab.com/project-dare/dare-platform/tree/v2.0).

Visit our [toy example](https://project-dare.gitlab.io/dare-platform/demo/) to interact with the DARE platform!
