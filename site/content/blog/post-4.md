---
title: DARE platform 3rd major Release
date: 2020-04-03
image: "images/dare_transparent.png"
---

DARE platform v3.0 release is now available at https://testbed.project-dare.eu/

In this version, there are changes in almost all major platform components. First of all,
the new testing environment (playground module) is updated to run in an nginx server
(instead of gunicorn) and is completely integrated with the AAI. In a similar way,
the Execution API is now based on nginx image and it is also integrated with the AAI.
The Provenance component have also some important changes, i.e. a more resilient queue,
unit tests on ProvenanceStore class, new search expression on API and in GUI. Finally,
the dispel4py library includes the following changes:

* The -f and -d arguments can be used to define the workflow inputs as shown in the workflow provenance. 
The -f argument accepts a path to a file containing the input dataset in JSON format. 
The -d argument accepts the input dataset in JSON format.
* The -f argument has priority over the -d argument.
* Fixed the issue inline provenance definition in the workflow script is used to create the workflow provenance when the argument --provenance-config is present. 
The --provenance-config has priority over the inline provenance definition now.
* Attention: "s-prov:WFExecutionsInputs" in --provenance-config is deprecated.

Finally, we have a completely new component integrated in the platform, the Execution Registry.
This component serves as backend to the Execution API handling the Shared File System, and more specifically,
the experiments & runs of the users as well as their uploads.

To download the v3.0 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.0)
 
