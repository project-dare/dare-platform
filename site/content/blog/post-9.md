---
title: DARE platform v3.5 release
date: 2020-10-14
image: "images/dare_transparent.png"
---

DARE platform v3.5 release is now available at https://platform.dare.scai.fraunhofer.de/

The v3.5 release contains various fixes and updates in the provenance component for the CWL workflows.
It also contains fixes in the Execution API and its helper functions as well as integration tests for
the components' APIs.

To download the v3.5 Release of the DARE platform visit our 
[GitLab Repository](https://gitlab.com/project-dare/dare-platform/-/tree/v3.5)
 
