# [DARE Platform](https://project-dare.gitlab.io/dare-platform/)

## Welcome 
The DARE platform is the integrated technical outcome of the [H2020 DARE project (#777413)
](http://project-dare.eu). The platform is documented in greater detail in its [dedicated GitLab microsite](https://project-dare.gitlab.io/dare-platform/).

The DARE platform is designed to address the requirements of research engineers and scientists, and empowers them to overcome technical and collaboration hurdles. It achieves these goals by exploiting the elasticity of the Cloud in resource acquisition and allocation. Crucially, it combines data provenance with other metadata and registries regarding datasets, experiments and resources to best enact workflows on present Cloud resources but also on remote and diverse platforms.

Using the DARE platform, users can design, implement and make available scientific workflows. They can subsequently execute them via the DARE API in order to power user-facing products, such as Virtual Research Environments, Web portals, etc. These workflows and their constituent processing elements are registered and available on the DARE repositories. These repositories can be used to find, communicate, replicate or debug existing methods. During execution DARE offers tunable provenance, which can help research engineers debug methods as well as site administrators locate bottlenecks and optimise local installations.

Whilst the DARE platform is being used in a number of use-cases, it is still a research product, and the DARE gitlab group an active collaboration tool between consortium members. As such, not all DARE repositories are being used as part of the deployable DARE platform. The ones used and their addresses are clearly listed below. 

DARE is a platform that should be deployed on a [Kubernetes](https://kubernetes.io)-enabled resource. In an operational setting it would require an administrator for deployment and day-to-day maintenance. Interactive use with the platform is typically provided via a [Jupyter Hub](https://jupyter.org). E.g. it could:

* support a Web application on a commercial cloud, or 
* it could form the basis of an Open Research solution at a research or academic site, or
* a combination of the above.

### More info
More information on the DARE platform, its components and how they relate, as well as its overall architecture and direction can be found in the following papers:

* I. Klampanos et al., "DARE: A Reflective Platform Designed to Enable Agile Data-Driven Research on the Cloud," 2019 15th International Conference on eScience (eScience), San Diego, CA, USA, 2019, pp. 578-585, doi: 10.1109/eScience.2019.00079. [[Read on IEEE Xplore]](https://ieeexplore.ieee.org/document/9041753)
 
* M. Atkinson et al., "Comprehensible Control for Researchers and Developers Facing Data Challenges," 2019 15th International Conference on eScience (eScience), San Diego, CA, USA, 2019, pp. 311-320, doi: 10.1109/eScience.2019.00042. [[Read on IEEE Xplore]](https://ieeexplore.ieee.org/document/9041709)


### Contributing to the DARE platform

The DARE platform and its components are published on an [Apache 2.0 License](https://www.apache.org/licenses/LICENSE-2.0). Everyone is welcome to download, deploy, and modify the source code, as well as to propose bug fixes and changes, either by creating issues or by contributing source code.
The most straightforward way to contribute code to the DARE platform and to its component repositories is by working on a fork and creating a pull request. 


## Deployment Instructions

The DARE platform consists of additional components, present in additional repositories in the DARE GitLab group. 

It is assumed that these components live in the same Kubernetes-managed cluster. Some components expose REST APIs to the outside world, while for some this is achieved via the DARE Execution API (see below). With some exceptions, the components are decoupled, with the DARE-platform and the Execution API repositories providing the glue. For instance, one could download and use dispel4py locally, but the Execution API makes it available as a service and manages a self-spawning MPI cluster on the Cloud.

The DARE components are documented in our [GitLab microsite](https://project-dare.gitlab.io/dare-platform/api-documentation/) with
instructions on how to use the DARE API. Additionally, there are available [installation instructions](https://project-dare.gitlab.io/dare-platform/configuration/).

In order to perform and integration test to the platform, check our toy example [here](https://project-dare.gitlab.io/dare-platform/demo/)

## Components

The DARE platform consists of a number of components developed by the partners of the DARE project, as well as of 3rd-party components. The DARE platform integrates the following internally-provided components.

### dispel4py
dispel4py is a Python library for describing abstract stream-based workflows for distributed data-intensive applications. It enables users to focus on their scientific methods, avoiding distracting details and retaining flexibility over the computing infrastructure they use.

It delivers mappings to diverse computing infrastructures, including cloud technologies, HPC architectures and  specialised data-intensive machines, to move seamlessly into production with large-scale data loads. More information can be found at the [dispel4py repository](https://gitlab.com/project-dare/dispel4py).

### s-ProvFlow
s-ProvFlow implements the P4 aspects of the DARE platform. It is a provenance framework for storage and access of data-intensive streaming lineage. It offers a a web API and a range of dedicated visualisation tools based on the underlying provenance model, S-PROV, which utilises and extends PROV and ProvONE models.

S-PROV addresses aspects of mapping between logical representation and concrete implementation of a workflow until its enactment onto a target computational resource.  The model captures aspects associated with the distribution of the computation, runtime changes and support for flexible metadata management and discovery for the data products generated by the execution of a data-intensive workflow.

Complete Documentation for the component can be found at the [s-ProvFlow repository](https://gitlab.com/project-dare/s-ProvFlow).

### dispel4py Registry
The dispel4py Registry is a RESTful Web service providing functionality for registering workflow entities, such as processing elements (PEs), functions and literals, while encouraging sharing and collaboration via groups and workspaces. More information is provided in the [dispel4py Registry repository](https://gitlab.com/project-dare/d4p-registry).

### CWL Workflow Registry
The CWL Workflow Registry provides a similar functionality as the Dispel4py Registry, with the difference that it is associated with CWL workflows. More information is provided at the [CWL workflow registry repository](https://gitlab.com/project-dare/workflow-registry).

### Data catalogue - Semantic Data Discovery
The Data catalogue is part of the DARE Knowledge Base and manages information related to the data elements processed via the platform. It exposes a RESTful API for registering new data sources and retrieving information on data previously registered or data results produced by processes executed over DARE. For
more information visit the [Data catalogue repository](https://gitlab.com/project-dare/semantic-data-discovery).

### DARE Execution API
The DARE Execution API enables the distributed and scalable execution of numerical simulations (now using SPECFEM3D code) and dispel4py workflows (e.g. used to describe the steps of RA except for simulations), which can be extended to other execution contexts. The Execution API also offers services such as uploading/downloading and referencing of data and process monitoring. For more information check out the [Execution API repository](https://gitlab.com/project-dare/exec-api).

### DARE playground 
The purpose of the playground is to provide an environment for testing and debugging purposes, especially dispel4py workflows. This helps users debug their methods before making them available for exection on the platform. More information is provided in the [DARE playground repository](https://gitlab.com/project-dare/playground).

