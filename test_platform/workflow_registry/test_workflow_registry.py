import os

from workflow_registry import helper_functions as hf

hostname = "https://platform.dare.scai.fraunhofer.de/workflow-registry"
login_hostname = "https://platform.dare.scai.fraunhofer.de/dare-login"

# Docker
docker_name = "test_platform"
docker_tag = "v1.0"
script_names = ["entrypoint.sh"]
path_to_docker_files = os.path.join(os.getcwd(), "docker_env")

# Workflow
workflow_name = "demo_workflow.cwl"
workflow_version = "v1.0"
spec_name = "spec.yaml"
workflow_part1_name = "arguments.cwl"
workflow_part1_version = "v1.0"
workflow_part2_name = "tar_param.cwl"
workflow_part2_version = "v1.0"
spec_part1_name = "arguments.yaml"
spec_part2_name = "tar_param.yaml"
path_to_workflow_files = os.path.join(os.getcwd(), "workflow_files")

workflow_part_data = [
    {"name": workflow_part1_name, "version": workflow_part1_version, "spec_name": spec_part1_name},
    {"name": workflow_part2_name, "version": workflow_part2_version, "spec_name": spec_part2_name}]


def update_counts(status_code, success, fail):
    if status_code == 200:
        success += 1
    else:
        fail += 1
    return success, fail


def test_docker_env(token, success, fail):
    print("Creating dockers")
    docker_env = hf.create_docker_env_with_scripts(hostname=hostname, token=token, docker_name=docker_name,
                                                   docker_tag=docker_tag, script_names=script_names,
                                                   path_to_files=path_to_docker_files)
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    docker_env = hf.create_docker_env(hostname=hostname, token=token, docker_name=docker_name,
                                      docker_tag="test_platform",
                                      docker_path=os.path.join(path_to_docker_files, "Dockerfile"))
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)

    print("=================================================")
    print("Updating docker")
    update = {"tag": "v1.1"}
    docker_env = hf.update_docker(hostname=hostname, token=token, update=update, docker_name=docker_name,
                                  docker_tag=docker_tag,
                                  path_to_docker=os.path.join(path_to_docker_files, "Dockerfile"))
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("=====================================================")
    print("Deleting docker")
    docker_env = hf.delete_docker(hostname=hostname, token=token, docker_name=docker_name,
                                  docker_tag="test_platform")
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("==================================================================")
    print("Update docker url")
    docker_env = hf.provide_url(hostname=hostname, token=token, docker_name=docker_name, docker_tag=update["tag"],
                                docker_url="https://gitlab.com/project-dare/dare-platform/workflow_registry:latest")
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("======================================================")
    print("Retrieving docker by name and tag")
    docker_env = hf.get_docker_by_name_and_tag(hostname=hostname, token=token, docker_name=docker_name,
                                               docker_tag=update["tag"])
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("============================================================")
    print("Retrieving docker by user")
    docker_env = hf.get_docker_by_user(hostname=hostname, token=token, username="admin")
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("============================================================")
    print("Downloading docker")
    docker_env = hf.download_docker(hostname, token, docker_name, update["tag"],
                                    local_path=os.path.join(os.getcwd(), "test", "test_platform.zip"))
    print(docker_env)
    success, fail = update_counts(docker_env[0], success, fail)
    print("=============================================================")
    return success, fail


def test_docker_script(token, success, fail):
    print("Adding new script to docker")
    script_name = "env_prep.sh"
    docker_script = hf.add_script_to_existing_docker(hostname=hostname, token=token, docker_name=docker_name,
                                                     docker_tag="v1.1", script_name=script_name,
                                                     path_to_script=path_to_docker_files)
    print(docker_script)
    success, fail = update_counts(docker_script[0], success, fail)
    print("============================================================================================")
    print("Editing an existing script in a docker")
    docker_script = hf.edit_script_in_existing_docker(hostname=hostname, token=token, docker_name=docker_name,
                                                      docker_tag="v1.1", script_name=script_name,
                                                      path_to_script=path_to_docker_files)
    print(docker_script)
    success, fail = update_counts(docker_script[0], success, fail)
    print("===========================================================================================")
    print("Retrieve a script by name and tag")
    docker_script = hf.get_script_by_name(hostname=hostname, token=token, docker_name=docker_name,
                                          docker_tag="v1.1", script_name=script_name)
    print(docker_script)
    success, fail = update_counts(docker_script[0], success, fail)
    print("==================================================================================")
    print("Download a script")
    docker_script = hf.download_script(hostname=hostname, token=token, docker_name=docker_name, docker_tag="v1.1",
                                       script_name=script_name,
                                       local_path=os.path.join(os.getcwd(), "test", script_name))
    print(docker_script)
    success, fail = update_counts(docker_script[0], success, fail)
    print("================================================================================================")
    print("Delete script from docker")
    docker_script = hf.delete_script_in_docker(hostname=hostname, token=token, docker_name=docker_name,
                                               docker_tag="v1.1", script_name="env_prep.sh")
    print(docker_script)
    success, fail = update_counts(docker_script[0], success, fail)
    print("===================================================================================================")
    return success, fail


def test_workflow(token, success, fail):
    print("Creating workflows")
    workflow = hf.create_workflow(hostname=hostname, token=token, workflow_name=workflow_name,
                                  workflow_version=workflow_version, spec_name=spec_name,
                                  path_to_cwls=path_to_workflow_files,
                                  docker_name=docker_name, docker_tag="v1.1", workflow_part_data=None)
    workflow2 = hf.create_workflow(hostname=hostname, token=token, workflow_name=workflow_name,
                                   workflow_version="test_platform", spec_name=spec_name,
                                   path_to_cwls=path_to_workflow_files,
                                   docker_name=docker_name, docker_tag="v1.1", workflow_part_data=None)
    success, fail = update_counts(workflow[0], success, fail)
    success, fail = update_counts(workflow2[0], success, fail)
    print(workflow)
    print(workflow2)
    print("==============================================================================================")
    print("Updating workflows")
    update = {"version": "v1.1"}
    workflow = hf.update_workflow(hostname=hostname, token=token, workflow_name=workflow_name,
                                  workflow_version=workflow_version, update=update,
                                  workflow_path=os.path.join(path_to_workflow_files, "demo_workflow.cwl"),
                                  specpath=os.path.join(path_to_workflow_files, "spec.yaml"))
    print(workflow)
    success, fail = update_counts(workflow[0], success, fail)
    print("=========================================================================================")
    print("Retrieving workflows")
    workflow = hf.get_workflow_by_name_and_version(hostname=hostname, token=token, workflow_name=workflow_name,
                                                   workflow_version=update["version"])
    print(workflow)
    success, fail = update_counts(workflow[0], success, fail)
    print("===============================================================================")
    print("Download workflow with docker")
    workflow = hf.download_workflow(hostname=hostname, token=token, workflow_name=workflow_name,
                                    workflow_version=update["version"],
                                    local_path=os.path.join(os.getcwd(), "test", "workflow.zip"))
    print(workflow)
    success, fail = update_counts(workflow[0], success, fail)
    print("=================================================================================")
    print("delete test_platform workflow")
    workflow = hf.delete_workflow(hostname=hostname, token=token, workflow_name=workflow_name,
                                  workflow_version="test_platform")
    print(workflow)
    success, fail = update_counts(workflow[0], success, fail)
    print("================================================================================")
    return success, fail


def test_workflow_part(token, success, fail):
    print("Creating workflow part - CWL class CommandLineTool")
    workflow_part = hf.add_workflow_part(hostname=hostname, token=token, workflow_name=workflow_name,
                                         workflow_version="v1.1", workflow_part_name=workflow_part1_name,
                                         workflow_part_version=workflow_part1_version,
                                         path_to_scripts=path_to_workflow_files, spec_name=spec_part1_name)
    print(workflow_part)
    success, fail = update_counts(workflow_part[0], success, fail)
    print("=========================================================")
    print("Updating workflow part")
    update = {"version": "v1.1"}
    workflow_part = hf.edit_workflow_part(hostname=hostname, token=token, workflow_name=workflow_name,
                                          workflow_version="v1.1", workflow_part_name=workflow_part1_name,
                                          workflow_part_version=workflow_part1_version, update=update,
                                          spec_name=spec_part1_name, path_to_files=path_to_workflow_files)
    print(workflow_part)
    success, fail = update_counts(workflow_part[0], success, fail)
    print("=================================================================")
    print("Retrieve workflow part")
    workflow_part = hf.get_workflow_part_by_name_and_version(hostname=hostname, token=token,
                                                             workflow_name=workflow_name,
                                                             workflow_version="v1.1",
                                                             workflow_part_name=workflow_part1_name,
                                                             workflow_part_version=update["version"])
    print(workflow_part)
    success, fail = update_counts(workflow_part[0], success, fail)
    print("=================================================================")
    print("Download workflow part")
    workflow_part = hf.download_workflow_part(hostname=hostname, token=token, workflow_name=workflow_name,
                                              workflow_version="v1.1", workflow_part_name=workflow_part1_name,
                                              workflow_part_version=update["version"],
                                              local_path=os.path.join(os.getcwd(), "test", "workflow.zip"))
    print(workflow_part)
    success, fail = update_counts(workflow_part[0], success, fail)
    print("=================================================================")
    print("Delete workflow part")
    workflow_part = hf.delete_workflow_part(hostname=hostname, token=token, workflow_name=workflow_name,
                                            workflow_version="v1.1", workflow_part_name=workflow_part1_name,
                                            workflow_part_version="v1.1")
    print(workflow_part)
    success, fail = update_counts(workflow_part[0], success, fail)
    print("==================================================================")
    return success, fail


def cleanup(hostname, token, success, fail):
    print("Cleanup workspace")
    response = hf.delete_docker(hostname=hostname, token=token, docker_name=docker_name, docker_tag="v1.1")
    print(response)
    success, fail = update_counts(response[0], success, fail)
    print("End of workflow registry test_platform")
    return success, fail


if __name__ == '__main__':

    test_success_count = 0
    test_fail_count = 0

    credentials = hf.read_credentials()
    auth_token = hf.login(hostname=login_hostname, username=credentials["username"], password=credentials["password"],
                          requested_issuer=credentials["issuer"])
    if credentials["ip"]:
        if credentials["workflow_port"]:
            hostname = "http://{}/{}".format(credentials["ip"], credentials["workflow_port"])
        if credentials["login_port"]:
            login_hostname = "http://{}/{}".format(credentials["ip"], credentials["login_port"])

    print("Auth token: {}".format(auth_token))
    if auth_token and type(auth_token) == dict:
        test_success_count += 1
        auth_token = auth_token["access_token"]
        test_success_count, test_fail_count = test_docker_env(auth_token, success=test_success_count,
                                                              fail=test_fail_count)
        test_success_count, test_fail_count = test_docker_script(auth_token, success=test_success_count,
                                                                 fail=test_fail_count)
        test_success_count, test_fail_count = test_workflow(auth_token, success=test_success_count,
                                                            fail=test_fail_count)
        test_success_count, test_fail_count = test_workflow_part(auth_token, success=test_success_count,
                                                                 fail=test_fail_count)
        test_success_count, test_fail_count = cleanup(hostname=hostname, token=auth_token, success=test_success_count,
                                                      fail=test_fail_count)
    else:
        test_fail_count += 1
        print("Login failed, could not perform any other tests!")
        exit(-1)
    print("Tests passed: {}".format(test_success_count))
    print("Tests failed: {}".format(test_fail_count))
