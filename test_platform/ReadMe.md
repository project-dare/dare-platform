# DARE platform testing

## General

The basic components of the DARE platform include: the dare login service, which uses Keycloak in the backend to 
authenticate the users, the two workflow registries, where users can register their workspaces and workflows and
then share it with other users, the Execution API, which depends on the two registries so as to retrieve a workflow and 
then execute it and finally the sprov API which is used so as to create/retrieve provenance logs. Below, 
we include some instructions on how to run the provided tests on those workflows.

## DARE login service

Although this service is tested through all the other tests, as no interaction with the platform can be performed
without a session token, we provide a separate test of the dare-login component. Before start testing, update
the credentials.yaml file only if you have a local installation of dare. In that case, use a username/password
of a user already registered in Keycloak and update the ip with the DARE platform's machine IP. Then, you need to find 
the port of the dare-login public service. To help you with this, we provide the dareports.sh script. Use it and
copy the port that is printed. Then paste it in the login_port field of the credentials.yaml

You can use the login test as a quick test, to check if the services are up and running. The login endpoint of the
DARE login service calls the d4p-registry, the cwl workflow registry, the execution API to initialize the user's
 environment as well as the Keycloak Service for authentication. The only main component that is not used in the login
 endpoint is the sprov API.

## CWL Workflow Registry component

Enter the workflow_registry folder to find the necessary files so as to test the Workflow Registry API.
If you have a local installation of DARE platform, you need to modify the credentials.yaml. Update the username/password fields
using a registered user in your local Keycloak. Then, you need to update the ip, login_port and workflow_port fields.
The ip field should contain the machine's IP while the login_port and workflow_port fields should contain the ports
of the public Kubernetes services for dare-login and workflow-registry. To find the ports use the dareports.sh script.
In case you do not have a local installation, the test will use the URL to the DARE's operational environment.

Once the credentials.yaml script is updated, execute the test_workflow_registry.py to test the endpoints of the
Workflow Registry API. When the script finishes, it will print the number of passed and failed tests. To test a CWL
execution go to the cwl_exec folder under the test_dare_components directory and execute the test_cwl.py. Of course,
modify again the yaml configuration file.

### CWL execution

The tests for the CWL execution are under the ```test_dare_components/cwl_exec``` directory.
The test_cwl.py script combines some functionality of the Workflow Registry API and the execution and monitoring 
functionality of the Execution API (test_cwl.py). 
The test_exec.py provides some additional tests on the Execution API once the CWL execution is finished. 

Keep in mind that in case you want to test a local DARE installation, you need to need to update the 
credentials.yaml file. You need a valid username/password from a registered user in you local Keycloak service
as well as the IP and ports of the services. Regarding the service ports, you can use the dareports.sh script
which prints the ports that you need for the cwl_exec test.

## Dispel4py Workflow Registry & Execution API

Enter the d4p_registry folder and prepare the credentials.yaml in a similar way as before. We provide a new dareports.sh
to find the d4p-registry port in your local installation. If you do not have a local installation, leave blank the ip,
login_port and workflow_port fields in the yaml file, so the test to use the operational DARE platform.

Use the test_d4p_registry to test the Dispel4py Registry API. Using this script, you can also test the execution of 
the dispel4py workflow. The dispel4py library installed is retrieved from [this repository](https://gitlab.com/project-dare/dispel4py) 
using the master branch.

For the testing of the execution result, use the test_exec.py. Modify the run_dir variable of the script with the run
directory that the execution step returned.

## Sprov API

To run the sprov tests, you need to enter the sprov container. To do so:

1. Use ```kubectl get pods``` command to list all the containers.
2. Find the sprov container and copy the name. Copy the simple sprov, not the sprov-db or sprov-cronjob or sprov-view.
3. Execute  ```kubectl exec -it <sprov name> -- apk add bash```
4. Enter the container: ```kubectl exec -it <sprov name> -- bash``` and:
    * cd ../test to enter the folder containing the tests.
    * Follow the instructions [here](https://gitlab.com/project-dare/s-ProvFlow/-/tree/master/provenance-api/src/test) to run manually the tests
 
The provenance API is used via the dispel4py test execution (d4p_registry folder).
To view the provenance traces use the sprov-viewer, i.e. http://IP:port/html/view.jsp or if you do not have a local
installation and you used the operational DARE platform you can enter [here](https://platform.dare.scai.fraunhofer.de/sprovflow-viewer/html/view.jsp).
You need of course to execute the above test in d4p-registry and then check the provenance traces.

Once on the S-ProvFlow viewer click on Open Run and in the popup, choose which of your executions you want to explore.
The viewer will show the workflow processes on the left panel with information such last active time,
data produced, execution node and messages such as logs and errors. Clicking on a process allows to see in detail what data has been produced.
Data are listed in the Data Product Panel. Each item includes the metadata, as specified in the workflow, its internal content, in case of larger
collections, and other general details, such as time of execution, process and associated messages.

Starting from a data element, it is possible to visualise the data-dependency graph. Here, you can explore the whole history (lineage)
of the data and processes that were involved in the generation of the data element. The navigation of the graph proceeds backwards,
from the data element of interest until the beginning of the analysis.