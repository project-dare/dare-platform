import json
import login_client as hf

login_hostname = "https://platform.dare.scai.fraunhofer.de/dare-login"

success = 0
fail = 0

if __name__ == '__main__':
    credentials = hf.read_credentials()
    auth_token = hf.login(hostname=login_hostname, username=credentials["username"], password=credentials["password"],
                          requested_issuer=credentials["issuer"])
    if credentials["ip"]:
        if credentials["workflow_port"]:
            hostname = "http://{}/{}".format(credentials["ip"], credentials["workflow_port"])
        if credentials["login_port"]:
            login_hostname = "http://{}/{}".format(credentials["ip"], credentials["login_port"])

    print("Auth token: {}".format(auth_token))
    if auth_token and type(auth_token) == dict:
        print("Successful login")
        success += 1
        token = auth_token["access_token"]
    else:
        fail += 1
        token = None
        print("Login failed")
    try:
        resp = hf.validate_token(hostname=login_hostname, token=token)
        print(resp)
        success += 1 if resp[0] == 200 else 0
        fail += 1 if resp[0] != 200 else 0
    except (BaseException, Exception):
        fail += 1
        resp = {}
        print("Request could not be completed")
    try:
        resp = json.loads(resp[1])
        issuer = resp["issuer"]
        refresh_token = auth_token["refresh_token"]
        resp = hf.refresh_token(hostname=login_hostname, refresh_token=refresh_token, issuer=issuer)
        refresh_resp = json.loads(resp[1])
        print("Access token: {}".format(refresh_resp["access_token"]))
        print("Refresh token: {}".format(refresh_resp["refresh_token"]))
        print("Expires in: {}".format(refresh_resp["expires_in"]))
        success += 1 if resp[0] == 200 else 0
        fail += 1 if resp[0] != 200 else 0
    except (BaseException, Exception):
        fail += 1
        print("Request for refreshing the token failed")
    print("Tests passed: {}".format(success))
    print("Tests failed: {}".format(fail))
