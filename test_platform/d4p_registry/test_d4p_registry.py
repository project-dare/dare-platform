from os import getcwd
from os.path import join

import helper_functions as hf

hostname = "https://platform.dare.scai.fraunhofer.de/d4p-registry"
login_hostname = "https://platform.dare.scai.fraunhofer.de/dare-login"
execution_hostname = "https://platform.dare.scai.fraunhofer.de/exec-api"
workflow_dir = "workflows"
path_to_workflow_dir = join(getcwd(), "workflows")
workflow_file = join(path_to_workflow_dir, "mySplitMerge_prov.py")
success = 0
fail = 0

if __name__ == '__main__':
    credentials = hf.read_credentials()
    if credentials["ip"]:
        if credentials["workflow_port"]:
            hostname = "http://{}/{}".format(credentials["ip"], credentials["workflow_port"])
        if credentials["login_port"]:
            login_hostname = "http://{}/{}".format(credentials["ip"], credentials["login_port"])
        if credentials["exec_port"]:
            execution_hostname = "http://{}/{}".format(credentials["ip"], credentials["exec_port"])
    auth_token = hf.login(hostname=login_hostname, username=credentials["username"], password=credentials["password"],
                          requested_issuer=credentials["issuer"])
    if auth_token and type(auth_token) == dict:
        success += 1
        print("Successful login!")
        token = auth_token["access_token"]

        print("Checking if cleanup is needed")
        try:
            hf.delete_workspace('mySplitWorkspace', hostname, token)
            print("Workspace is now cleaned!")
            success += 1
        except (BaseException, Exception):
            pass

        # Register a workspace
        print("Creating a new workspace")
        try:
            workspace_url, workspace_id = hf.create_workspace("", "mySplitWorkspace", "", hostname, token)
            workspace_id = int(workspace_id)
            success += 1
            print('Workspace created with ID: ' + str(workspace_id))
            try:
                # Register ProcessingElementSignature
                pe_url = hf.create_pe(desc="", name="mySplitMerge", conn=[], pckg="mysplitmerge_pckg",
                                      workspace=workspace_url, clone="", peimpls=[], hostname=hostname, token=token)
                print("PESig URL: {}/pes/{}".format(hostname, str(pe_url.split('/')[4])))
                success += 1
                try:
                    # Register ProcessingElementImplementation
                    with open(workflow_file, "r") as f:
                        code = f.read()

                    impl_id = hf.create_peimpl(desc="", code=code, parent_sig=pe_url, pckg="mysplitmerge_pckg",
                                               name="mySplitMerge", workspace=workspace_url, clone="",
                                               hostname=hostname, token=token)
                    print('PE Implementation ID: ' + str(impl_id))
                    success += 1
                    print("Execute workflow")
                    resp = hf.submit_d4p(impl_id=impl_id, pckg="mysplitmerge_pckg", workspace_id=workspace_id,
                                         pe_name="mySplitMerge",
                                         reqs='https://gitlab.com/project-dare/exec-api/-/raw/master/examples/mySplitMerge/scripts/reqs.txt',
                                         token=token, hostname=execution_hostname, n_nodes=6, no_processes=6,
                                         iterations=1)
                    print(resp)
                    success += 1
                    print("Monitoring the containers to check when execution finishes")
                    hf.monitor(token=token, hostname=execution_hostname)
                    print("Once the execution is finished use the test_exec.py to find the output of the execution")
                    print("Get the run dir from here: {}".format(resp))
                    print("Tests passed: {}".format(success))
                    print("Tests failed: {}".format(fail))
                    exit(0)
                except (BaseException, Exception):
                    print("Could not create PE implementation")
                    fail += 1
            except (BaseException, Exception):
                print("Could not create PE")
                fail += 1
        except (BaseException, Exception):
            print("Creating workspace failed")
            fail += 1
    else:
        print("Login failed, so no more tests can be executed")
        fail += 1
        exit(-1)

