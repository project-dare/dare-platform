import json
import helper_functions as hf

login_hostname = "https://platform.dare.scai.fraunhofer.de/dare-login"
execution_hostname = "https://platform.dare.scai.fraunhofer.de/exec-api"
run_dir = ""
success = 0
fail = 0

if __name__ == '__main__':
    credentials = hf.read_credentials()
    if credentials["ip"]:
        if credentials["login_port"]:
            login_hostname = "http://{}/{}".format(credentials["ip"], credentials["login_port"])
        if credentials["exec_port"]:
            execution_hostname = "http://{}/{}".format(credentials["ip"], credentials["exec_port"])
    auth_token = hf.login(hostname=login_hostname, username=credentials["username"], password=credentials["password"],
                          requested_issuer=credentials["issuer"])
    if auth_token and type(auth_token) == dict:
        success += 1
        print("Successful login!")
        token = auth_token["token"]
        print("Listing the directories")
        try:
            resp = hf.list_folders(token=token, hostname=execution_hostname)
            hf.folders_pretty_print(json.loads(resp))
            success += 1
        except (BaseException, Exception):
            fail += 1
            print("Request failed")
        try:
            print("Listing the output in the run dir")
            folder_path = "/home/mpiuser/sfs/{}/runs/{}/output".format(credentials["username"], run_dir)
            resp = hf.list_folder_files(token=token, path=folder_path, hostname=execution_hostname)
            hf.files_pretty_print(json.loads(resp))
            success += 1
        except (BaseException, Exception):
            print("Request failed")
            fail += 1
        print("Tests passed: {}".format(success))
        print("Tests failed: {}".format(fail))
    else:
        fail += 1
        print("Could not login. No more tests can be performed")
        exit(-1)
