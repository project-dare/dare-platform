helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/
helm repo update

kubectl create namespace jupyterhub
helm upgrade --install jupyterhub jupyterhub/jupyterhub --namespace jupyterhub --version=0.9.0 --values jupyterhub-config.yaml
